(defproject pixel-sorting "1.0"
  :description "Pixel sorting experiment"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [quil "2.7.1" :exclusions [org.clojure/clojure]]
                 [org.apache.commons/commons-math3 "3.6.1"]
                 [org.clojure/tools.cli "0.4.1"]
                 [incanter "1.9.3"]]
  :jvm-opts ["-Xms1100m" "-Xmx1100M" "-server"]
  :aot [sketch.dynamic])
