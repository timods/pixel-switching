Pixel swapping

## Future work

Implement a more efficient data structure for storing the regions we've already processed. There exist real-world data structures for this, but it'd be fun to design my own anyways. Here's my thought process:

- Coordinate pairs are added once they are processed
- I need to be able to efficiently search this data structure to see whether a pariticular pair has been processed already (hence the current set-based implementation)
- It should be memory efficient: why store all pixels from {:x I :y N} to {:x J :y N} individually when that can be compactly represented as a range {:x-from I :y-from N :x-to (- J I) :y-to N}. This can be done to grow rectangular regions as well, but that's more complicated (rows of equal width are trivial to compact)

This isn't exactly an optimal algorithm: it doesn't reason about how to break up existing regions in order to better coalesce chunks. It'd be rather easy to get to a point where we have inefficient chunks.

Instead, since the region size is known ahead of time it can be recursively subdivided into, say, 4 quadrants at each layer. Each quadrant compresses rows within itself, giving them a maxium length (and allowing for intelligent row compaction). Then full quadrants can be combined together to further minimize the memory usage (and comparisons necessary)

This may be inefficient compared to a set, but it'll be fun to build anyways (and allow this to scale to huge regions).


**NOTE** this is pretty much the trivial case for a k-d tree
