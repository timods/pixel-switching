(ns sketch.runcore
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [clojure.tools.cli :refer [parse-opts]]
            [sketch.dynamic :as dynamic])
  (:gen-class))

(def cli-options
  ;; An option with a required argument
  [["-w" "--width WIDTH" "Image width"
    :default 600
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 10800) "Must be a number between 0 and 65536"]]
  ["-g" "--height HEIGHT" "Image height"
    :default 1000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 10800) "Must be a number between 0 and 65536"]]
  ["-r" "--random-seed RAND" "Random seed"
    :default 1000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
  ["-t" "--times TIMES" "Number of times to repeat the process"
    :default 1000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ;; A non-idempotent option (:default is applied first)
   ;; A boolean option defaulting to nil
   ["-h" "--help"]])

(defn -main [& args]
  (q/sketch
   :title "Big Image"
   :setup dynamic/setup
   :update dynamic/update-state
   :draw dynamic/draw-state
   :key-pressed dynamic/key-pressed
   :middleware [m/fun-mode]
   :size [480 679]
   :features [:exit-on-close :no-safe-fns]))
