(ns sketch.dynamic
  (:require [quil.core :refer :all])
  (:use [incanter.core :only [$=]])
  (:use [clojure.math.combinatorics :only [combinations cartesian-product]])
  (:use [clojure.pprint])
  (:use [clojure.string :only [join split]])
  (:use [clojure.set :only [union difference]])
  (:import [org.apache.commons.math3.distribution ParetoDistribution])
  (:import [processing.core PShape PGraphics]))

(def N 10)
(def M 30)

(defn neighborhood-tile [x y]
  "Return a tile for the neighborhood of the given pixel. This returns the central pixel itself as well"
  (let [w (width)
        h (height)
        nw (if (= x (- w 1)) 2 3)
        nh (if (= y (- h 1)) 2 3)]
    (get-pixel (- x 1) (- y 1) nw nh)
    )
  )
(defn compare-to-tile [tile p]
  "Compare a pixel to a tile, where the similarity is defined as the sum of the absolute difference in pixel RGB values"
  (reduce + 0 (map (fn [p2] (abs (- p p2))) (.pixels tile))))

(defn neighbor-coordinates [x y]
  "Return all in-bounds neighboring coordinate pairs of (x, y)"
  (let [w (width)
        h (height)
        candidates [{:x x :y (- y 1)}
                    {:x x :y (+ y 1)}
                    {:x (- x 1) :y y}
                    {:x (- x 1) :y (- y 1)}
                    {:x (- x 1) :y (+ y 1)}
                    {:x (+ x 1) :y y}
                    {:x (+ x 1) :y (- y 1)}
                    {:x (+ x 1) :y (+ y 1)}]]
    (filter (fn [p]
              (let [{x :x y :y} p]
                (and (>= x 0) (< x w) (>= y 0) (< y h))))
            candidates
            )))

(defn unprocessed-neighbors [processed coords]
  "Return all unprocessed neighbor coordinates of the specified base coordinates.
   If coords is empty, return the center of the image"
  (if (empty? coords)
    (set [{:x (round (/ (width) 2)) :y (round (/ (height) 2))}])
    (filter (fn [coord] (not (contains? processed coord)))
            (into #{} (flatten (map (fn [c] (neighbor-coordinates (:x c) (:y c))) coords))))))

(defn n-unprocessed [processed]
  "Return the coordinates of M random unprocessed pixels"
  (take M (filter #(not (contains? processed %)) (repeatedly (fn [] {:x (floor (random (width)))
                                                                     :y (floor (random (height)))})))))

(defn setup []
  (color-mode :hsb 360 100 100 1.0)
  (let [img (load-image "data/Moebius-4.jpg")
        outdir (join "" ["output/" (.format (java.text.SimpleDateFormat. "MM-dd-yyyy_HH.mm.ss") (new java.util.Date))])
        ]
    (.mkdir (java.io.File. outdir))
    {:image img
     :iter 0
     :trial 0
     :todo (set [])
     :next (set [])
     :processed (set [])
     :mode :dfs
     :comparator :most-similar
     :outdir outdir
     }
    )
  )

(defn update-state [state]
  "given the old state, return the new state"
  (if (= (:iter state) 0)
    ;; Give it a few tries to resize the background properly...
    (update-in state [:iter] inc)
    (let [iter (:iter state)
          last-iter (:next state)
          todo (:todo state)
          processed (union (:processed state) last-iter)
          candidates (case (:mode state)
                       ;; TODO: this could be much faster by filtering out coordinate pairs
                       ;; that are completely surrounded
                       :bfs (into #{} (unprocessed-neighbors processed processed))
                       ;; DFS-ish is unprocessed-neighbors over last-iter
                       :dfs (into #{} (unprocessed-neighbors processed last-iter)))
          this-iter (->> (if (empty? candidates)
                           ;; Time to look elsewhere
                             (into #{} (unprocessed-neighbors processed processed))
                             candidates)
                         shuffle
                         (transduce (take (+ N iter)) conj))]
      (-> state
          ;; Get the next coords to swap
          (assoc-in [:processed] processed)
          (assoc-in [:next] (union this-iter todo))
          (assoc-in [:todo] (set []))
          (update-in [:iter] inc)
          ))
    )
  )

(defn draw-state [state]
  (if (= (:iter state) 0)
    (let [img (:image state)]
      (do
        (resize-sketch (.width img) (.height img))
        (image img 0 0)))
    (let [{:keys [trial next outdir processed iter]} state
          processed-plus-next (union processed (into #{} next))
          pxls (pixels)
          w (width)
          h (height)]
      (if (not (empty? next))
        (do
         (doseq [p next]
           (let [{:keys [x y]} p
                 neighborhood (neighborhood-tile x y)
                 random-coords (into [] (n-unprocessed processed-plus-next))
                 comparator (fn [p2] (compare-to-tile neighborhood (get-pixel (:x p2) (:y p2))))
                 stream (mapv #(assoc-in % [:s] (comparator %)) random-coords)
                 target (case (:comparator state)
                          :most-similar (apply min-key :s stream)
                          :least-similar (apply max-key :s stream)
                          (apply min-key :s stream))
                 x2 (:x target)
                 y2 (:y target)
                 loc1 (+ x (* y w))
                 loc2 (+ x2 (* y2 w))
                 c (aget pxls loc1)
                 c2 (aget pxls loc2)]
               (aset-int pxls loc1 c2)
               (aset-int pxls loc2 c)))
         (update-pixels)
         ;;(save-frame (join "" [outdir "/trial-" trial "-frame-####.png"]))
         )
        (do
          (save (join "" [outdir "/trial-" trial ".png"]))
          (no-loop))))))

(defn key-pressed-inner [state event]
  (let [img (case (:key event)
              :1 (load-image "data/gustave_dore_inferno.jpg")
              :2 (load-image "data/Moebius-1.jpg")
              :3 (load-image "data/Moebius-2.jpg")
              :4 (load-image "data/Moebius-3.jpg")
              :5 (load-image "data/Moebius-4.jpg")
              :6 (load-image "Ito_Jakuchu_AjisaiSoukei-zu.jpg")
              ;; Fallthrough state
              (:image state))
        comparator (case (:key event)
                     :m :most-similar
                     :l :least-similar
                     (:comparator state))
        mode (case (:key event)
               :d :dfs
               :b :bfs
               (:mode state))]
    (resize-sketch (.width img) (.height img))
    (-> state
        (assoc-in [:mode] mode)
        (assoc-in [:comparator] comparator)
        (assoc-in [:image] img)
        (assoc-in [:iter] -1)
        (assoc-in [:next] (set []))
        (assoc-in [:processed] (set []))
        (update-in [:trial] inc)
        )
    )
  )

(defn key-pressed [state event]
  (redraw)
  (start-loop)
  ;; I need to figure out a better way of doing this
  (case (:key event)
    (:1 :2 :3 :4 :5 :6 :m :l :b :d) (key-pressed-inner state event)))

(defn mouse-released [state event]
  (redraw)
  (start-loop)
  (-> state
      (assoc-in [:todo] (set [event]))))
