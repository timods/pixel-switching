(ns sketch.core
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [sketch.dynamic :as dynamic])
  (:gen-class))

(q/defsketch example
  :title "Pixel sorting"
  ;;:renderer :p2d
  :setup dynamic/setup
  :update dynamic/update-state
  :draw dynamic/draw-state
  :middleware [m/fun-mode]
  :key-pressed dynamic/key-pressed
  :mouse-released dynamic/mouse-released
  :size [582 950])

(defn refresh []
  (use :reload 'sketch.dynamic)
  (.loop example))
